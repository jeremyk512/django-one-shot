from django.urls import path
from todos.views import (
    todo_list_view,
    todo_list_detail_view,
    todo_list_create_view,
    todo_list_update_view,
    todo_list_delete_view,
    todo_item_create_view,
    todo_item_update_view,
)


urlpatterns = [
    path("", todo_list_view,
         name="todo_list_list"),
    path("<int:id>/", todo_list_detail_view,
         name="todo_list_detail"),
    path("create/", todo_list_create_view,
         name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update_view,
         name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete_view,
         name="todo_list_delete"),
    path("items/create/", todo_item_create_view,
         name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update_view,
         name="todo_item_update"),
]
