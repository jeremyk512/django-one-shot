from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list_view(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
        }
    return render(request, "todos/list.html", context)


def todo_list_detail_view(request, id):
    detail = TodoList.objects.get(id=id)
    context = {
        "detail": detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update_view(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete_view(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create_view(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


def todo_item_update_view(request, id):
    list = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=list)
    context = {
        "form": form,
        "todo_object": list,
    }
    return render(request, "todos/item_update.html", context)
